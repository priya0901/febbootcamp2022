package week1;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.github.bonigarcia.wdm.WebDriverManager;

public class EditOpportunity {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		WebDriverManager.chromedriver().setup();
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--disable-notifications");
		ChromeDriver driver = new ChromeDriver(options);
		
		//Login to the application
		driver.get("https://login.salesforce.com");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(Duration.ofMillis(6000));
		driver.findElement(By.id("username")).sendKeys("makaia@testleaf.com");
		driver.findElement(By.xpath("//input[@id = 'password']")).sendKeys("BootcampSel$123");
        driver.findElement(By.xpath("//input[@id ='Login']")).click();
        
        //Click on toggle menu button from the left corner
        driver.findElement(By.xpath("//div[@class='slds-icon-waffle']")).click();
        
        // Click view All and click Sales from App Launcher
        driver.findElement(By.xpath("//button[text()='View All']")).click();
        driver.findElement(By.xpath("//p[@title ='Manage your sales process with accounts, leads, opportunities, and more']")).click();  
       
        // Click on Opportunity tab 
        WebElement opp =  driver.findElement(By.xpath("//a[@title='Opportunities']"));
        driver.executeScript("arguments[0].click();", opp);
       
        //Search the Opportunity 'Salesforce Automation by Your Name'
        String titleName = "Salesfoce Automation By PriyaMurali";
        Thread.sleep(5000);
        driver.findElement(By.xpath("//input[@name='Opportunity-search-input']")).sendKeys(titleName);
        driver.findElement(By.xpath("//input[@name='Opportunity-search-input']")).sendKeys(Keys.ENTER);
        Thread.sleep(5000);
        driver.findElement(By.xpath("//a[@title='"+titleName+"']")).click();
        
        // Click on the Dropdown icon and Select Edit
        Thread.sleep(5000);
        WebElement icon =driver.findElement(By.xpath("//span[text()='Show more actions']"));
        JavascriptExecutor js = (JavascriptExecutor)driver;
        js.executeScript("arguments[0].click();", icon);
        Thread.sleep(5000);
        WebElement edit = driver.findElement(By.xpath("//span[text()='Edit']"));
        js.executeScript("arguments[0].click();", edit);
        
        // Choose close date as Tomorrow date
        driver.findElement(By.xpath("//input[@name='CloseDate']")).clear();
        Thread.sleep(5000);
        driver.findElement(By.xpath("//input[@name='CloseDate']")).sendKeys("3/28/2022");
        
        //Select 'Stage' as Perception Analysis
        Thread.sleep(5000);
        WebElement stage =  driver.findElement(By.xpath("//label[text()='Stage']/following-sibling::div[1]//button"));
        js.executeScript("arguments[0].click();", stage);
        String stageName = "Perception Analysis";
        Thread.sleep(5000);
        WebElement stageopt = driver.findElement(By.xpath("//span[@title='"+stageName+"']"));
        js.executeScript("arguments[0].scrollIntoView();", stageopt);
        js.executeScript("arguments[0].click();", stageopt);
        
        
        //Select Deliver Status as In Progress
        Thread.sleep(5000);
        WebElement install = driver.findElement(By.xpath("//label[text()='Delivery/Installation Status']//following-sibling::div[1]//button"));
        js.executeScript("arguments[0].scrollIntoView();", install);
        js.executeScript("arguments[0].click();", install);
        WebElement installopt= driver.findElement(By.xpath("//span[@title='In progress']"));
        js.executeScript("arguments[0].click();", installopt);
        
        //Enter Description as SalesForce
        Thread.sleep(5000);
        driver.findElement(By.xpath("//textarea[@class='slds-textarea']")).sendKeys("Edited");
        WebElement save =driver.findElement(By.xpath("//button[@name='SaveEdit']"));
        js.executeScript("arguments[0].click();", save);
        
        //Click on Details
        WebElement details = driver.findElement(By.xpath("//a[text()=\"Details\"]"));
        js.executeScript("arguments[0].click();", details);
        
        //verify the stage name
		WebElement name = driver.findElement(By.xpath("//span[text()='Lead Source']/preceding::lightning-formatted-text[1]"));
        System.out.println(name.getText());
        String stagever = (name.getText().contains(stageName)) ? "Stage name verified" : "Stage name not verified ";
        System.out.println(stagever);
	}

}
